package com.t1.yd.tm.command.task;

import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_show_by_id";

    @NotNull
    public static final String DESCRIPTION = "Show task by Id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");


        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findTaskById(userId, id);

        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
