package com.t1.yd.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class Command {

    @Nullable
    private String argument;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    @Override
    public String toString() {
        return String.format("%s%s : %s", name, argument != null ? ", " + argument : "", description);
    }

}
